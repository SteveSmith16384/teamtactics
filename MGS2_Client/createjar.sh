rm teamtactics.jar
./compile.sh
jar cvfm teamtactics.jar manifest.txt -C bin .
jar uvf teamtactics.jar data
cd libs
cp *.jar ..
cd ..
zip teamtactics teamtactics.jar tritonus_share.jar start_linux.sh start_windows.bat readme.txt update_linux_version.sh update_windows_version.bat
rm *.jar

