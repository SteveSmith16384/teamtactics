TEAM TACTICS

By Stephen Carlyle-Smith (stephen.carlylesmith@googlemail.com)

WHAT IS IT?
A Multiplayer squad tactical game


HOW TO START
You will need Java.  You should be able to run teamtactics.jar.  If this doesn't work, try running either start_linux.sh or start_windows.bat.


CONTROLS
W, A, S, D to move
Left mouse button to shoot
Q for game stats
C for chat window
, to change unit type


UPDATING
If you need to (or want to) update to the latest version, you can run either update_window.bat or update_linux.bat.


Please feel free to email me at stephen.carlylesmith@googlemail.com with any questions, problems or suggestions.  Or praise.

