package mgs2.client.datastructs;

import java.util.ArrayList;
import java.util.HashMap;

import mgs2.shared.GameStage;

public class SimpleGameData {
	
	public int id;
	public GameStage game_stage;
	public String game_name;
	public byte winning_side;
	private byte sides;
	public HashMap<Integer, String> side_names = new HashMap<Integer, String>();
	public ArrayList<SimplePlayerData> players[];// = new ArrayList<SimplePlayerData>(); 

	public SimpleGameData() {
	}
	
	
	public byte getNumSides() {
		return sides;
	}
	
	
	public void setNumPlayerSides(byte s) {
		this.sides = s;
		 players = new ArrayList[s+1]; 
		 for (int i=0 ; i<=s ; i++) {
			 players[i] = new ArrayList<SimplePlayerData>();
		 }
	}
	
	
	public int getNumPlayerInSide(byte s) {
		return players[s].size();
	}
	
	
	public void setSimplePlayerData(SimplePlayerData data) {
		//if (data.side > 0) {
			this.players[data.side].add(data);
		//}
	}
	
	
}
