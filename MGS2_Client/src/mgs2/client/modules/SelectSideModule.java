package mgs2.client.modules;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.io.IOException;

import mgs2.client.ClientMain;
import mgs2.client.ClientWindow;
import mgs2.client.gfx.DroppingText;
import mgs2.shared.Statics;
import mgs2.shared.Statics.GameType;
import ssmith.awt.AWTFunctions;
import ssmith.lang.NumberFunctions;

public final class SelectSideModule extends AbstractModule {

	private String msg = "";
	private DroppingText droptext;
	//private Image background;

	public SelectSideModule(ClientMain _main, ClientWindow window) throws IOException {
		super(_main, window);

		droptext = new DroppingText(window, "team_tactics_logo.png");

		if (Statics.GAME_TYPE == GameType.GAUNTLET) {
			main.setSide((byte)1);
		}
	}


	@Override
	public void gameLoop(Graphics g, long interpol) throws IOException {
		g.setColor(Color.white);

		/*if (background != null) {
			g.drawImage(background, 0, 0, window);
		}*/

		droptext.paint(g, interpol);

		if (main.game_data != null) {
			g.setFont(window.font_large);
			AWTFunctions.DrawString(g, main.game_data.game_name, 20, 70);
			g.setFont(window.font_normal);
			AWTFunctions.DrawString(g, "Select side 1 - " + main.game_data.getNumSides() + "", 20, 100);
			for (byte side=1 ; side<=main.game_data.getNumSides() ; side++) {
				int num = main.game_data.getNumPlayerInSide(side);
				AWTFunctions.DrawString(g, main.game_data.side_names.get((int)side) + ", currently " + num + " players on this side", 20, 100+(side*30));
			}
			g.setColor(Color.yellow);
			g.drawString(msg, 20, 200);

			// Check for keypresses
			for (byte i=1 ; i<=main.game_data.getNumSides() ; i++) {
				if (window.keys[KeyEvent.VK_1-1+i]) {
					window.keys[KeyEvent.VK_1-1+i] = false; // Prevent repeats
					if (checkNumPlayers(i)) {
						main.setSide(i);
						return;
					}
				}
			}
			if (main.bot != null) {
				main.setSide(NumberFunctions.rndByte(1, 2));

			}
		}
	}


	private boolean checkNumPlayers(byte side) {
		int num = main.game_data.getNumPlayerInSide(side);
		for (byte i=1 ; i<=main.game_data.getNumSides() ; i++) {
			if (i != side) {
				int thisside = main.game_data.getNumPlayerInSide(i);
				if (num > thisside) {
					msg = "Too many players on side " + side;
					return false;
				}
			}
		}
		return true;
	}


}
