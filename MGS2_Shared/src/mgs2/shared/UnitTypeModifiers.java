package mgs2.shared;


public class UnitTypeModifiers {

	public UnitTypeModifiers() {
	}
	
	
	public static final float GetSpeedMod(UnitType unit) {
		switch (unit) {
		case DOCTOR:
			return 1.1f;
		case RECON:
			return 1.3f;
		case GUNNER:
			return 0.6f;
		case UNSET:
			throw new RuntimeException("Unit type not set");
		default:
			return 1;
		}
	}


	public static final int GetShotInterval(UnitType unit) {
		switch (unit) {
		case SNIPER:
			return 3000;
		case GUNNER:
			return 300;
		case DOCTOR:
			return 200;
		case UNSET:
			throw new RuntimeException("Unit type not set");
		default:
			return 500;
		}
	}


	public static final int GetBulletMaxDamage(UnitType unit) {
		switch (unit) {
		case RECON:
			return 10;
		case DOCTOR:
			return 10;
		case GUNNER:
			return 20;
		case ASSASSIN:
			return 40;
		case SNIPER:
			return 50;
		case UNSET:
			throw new RuntimeException("Unit type not set");
		default:
			return 15;
		}
	}


	public static final int GetBulletMinDamage(UnitType unit) {
		switch (unit) {
		case SNIPER:
			return 30;
		case ASSASSIN:
			return 20;
		case UNSET:
			throw new RuntimeException("Unit type not set");
		default:
			return 5;
		}
	}


	public static final int GetBulletRange(UnitType unit) {
		switch (unit) {
		case SNIPER:
			return Statics.SQ_SIZE * 20;
		case ASSASSIN:
		case DOCTOR:
			return Statics.SQ_SIZE * 4;
		case UNSET:
			throw new RuntimeException("Unit type not set");
		default:
			return Statics.SQ_SIZE * 8;
		}
	}


	public static final int GetMaxHealth(UnitType unit) {
		int std_health = 100;
		if (Statics.DEBUG) {
			std_health = 10;
		}
		switch (unit) {
		case DOCTOR:
		case RECON:
			return (int)(std_health * 0.75f);
		case GUNNER:
			return (int)(std_health * 1.5f);
		case UNSET:
			throw new RuntimeException("Unit type not set");
		default:
			return std_health;
		}
	}


	public static final float GetBulletThickness(UnitType unit) {
		switch (unit) {
		case DOCTOR:
		case GUNNER:
			return 6f;
		case SNIPER:
			return 3f;
		case UNSET:
			throw new RuntimeException("Unit type not set");
		default:
			return 3f;
		}
	}


}
