package mgs2.shared;


public enum UnitType {

	UNSET(0), 
	DOCTOR(1), 
	RECON(2),
	ASSASSIN(3),
	TROOPER(4),
	SNIPER(5),
	GUNNER(6),
	STERNER(7);
	
	private byte theID;

	UnitType( int pID )
	{
		theID = (byte)pID;
	}

	public byte getID()
	{
		return this.theID;
	}

	public static UnitType get( final byte pID )
	{
		UnitType lTypes[] = UnitType.values();
		for( UnitType lType : lTypes )
		{
			if( lType.getID() == pID )
			{
				return lType;
			}
		}
		throw new IllegalArgumentException("Unknown type: " + pID);
	}
	
}
