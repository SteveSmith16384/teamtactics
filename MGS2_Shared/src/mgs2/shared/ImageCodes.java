package mgs2.shared;

public final class ImageCodes {

	// Player codes
	public static final int SIDE_1 = 1;
	public static final int SIDE_2 = 2;
	public static final int SIDE_3 = 3;
	public static final int SIDE_4 = 4;
	//public static final int STERNER = 5;
	
	
	// Texture types
	public static final int TEX_INTERIOR1 = 1;
	public static final int TEX_MOONROCK = 2;
	public static final int TEX_CORRIDOR1 = 3;
	public static final int TEX_INTERIOR2 = 4;
	public static final int TEX_RUBBLE = 5;
	public static final int TEX_PATH = 6;
	public static final int TEX_GRASS = 7;
	public static final int TEX_ROAD = 8;
	public static final int TEX_MUD = 9;
	public static final int TEX_WOODEN_FLOOR = 10;
	public static final int TEX_WATER = 11;
	public static final int TEX_BEACH = 12;
	public static final int TEX_SAND1 = 13;
	public static final int TEX_SAND2 = 14;
	public static final int TEX_WOODEN_PLANKS = 15;
	public static final int TEX_MOONBASE_BLUE = 16;
	public static final int TEX_CORRUGATED_WALL = 17;
	public static final int TEX_MUDTRACK_EW = 18;
	public static final int TEX_MUDTRACK_NS = 19;
	public static final int TEX_RAILWAY_EW = 20;
	public static final int TEX_RAILWAY_NS = 21;
	public static final int TEX_MUDTRACKS_RB = 22;
	public static final int TEX_MUDTRACKS_LT = 23;
	public static final int TEX_SNOW = 24;
	public static final int TEX_SPACEWALL = 25;
	public static final int TEX_STONE_TILES = 26;
	public static final int TEX_MUDTRACKS_RT = 27;
	public static final int TEX_CARPET1 = 28;
	public static final int TEX_WOODEN_FLOOR2 = 29;
	public static final int TEX_SECTOR1 = 30;
	public static final int TEX_STREET = 31;
	public static final int TEX_STREET_ZEBRA_LR = 32;
	public static final int TEX_MUDTRACKS_UD = 33;
	public static final int TEX_MUDTRACKS_UDR = 34;
	public static final int TEX_MUDTRACKS_LR = 35;
	public static final int TEX_MUDTRACKS_LB = 36;
	public static final int TEX_STREET_ZEBRA_UD = 37;
	public static final int TEX_YELLOW_LINE_INNER_TR = 38;
	public static final int TEX_YELLOW_LINE_INNER_TL = 39;
	public static final int TEX_YELLOW_LINE_INNER_BR = 40;
	public static final int TEX_YELLOW_LINE_INNER_BL = 41;
	public static final int TEX_YELLOW_LINE_OUTER_BL = 42;
	public static final int TEX_YELLOW_LINE_OUTER_BR = 43;
	public static final int TEX_YELLOW_LINE_OUTER_TL = 44;
	public static final int TEX_YELLOW_LINE_OUTER_TR = 45;
	public static final int TEX_YELLOW_LINE_L = 46;
	public static final int TEX_YELLOW_LINE_R = 47;
	public static final int TEX_YELLOW_LINE_T = 48;
	public static final int TEX_YELLOW_LINE_B = 49;
	public static final int TEX_ALIEN_SKIN = 50;
	public static final int TEX_BULKHEAD = 51;
	public static final int TEX_SPACESHIP_WALL = 52;
	public static final int TEX_TELEPORTER = 53;
	public static final int TEX_COUCH = 54;
	public static final int TEX_METAL_FLOOR5 = 55;
	public static final int TEX_METAL_FLOOR6 = 56;
	public static final int TEX_METAL_FLOOR15 = 57;
	public static final int TEX_HEDGE = 58;
	public static final int TEX_FLOORHATCH = 59; // New
	public static final int TEX_METAL_FLOOR41 = 60;
	public static final int TEX_LAB_FLOOR1 = 61;
	public static final int TEX_LAB_FLOOR2 = 62;
	public static final int TEX_ROAD_WHITE_LINE_UD = 63;
	public static final int TEX_ROAD_WHITE_LINE_LR = 64;
	public static final int TEX_SECTOR2 = 65;
	public static final int TEX_SECTOR3 = 66;
	public static final int TEX_SECTOR4 = 67;
	public static final int TEX_ESCAPE_HATCH = 68;
	public static final int TEX_BRICKS = 69;
	public static final int TEX_WHITE_LINE_LR = 70;
	public static final int TEX_WHITE_LINE_UD = 71;
	public static final int TEX_RUBBLE_2 = 72; // Not used - only in Android
	public static final int TEX_RUBBLE_3 = 73; // Not used - only in Android
	public static final int TEX_RUBBLE_4 = 74; // Not used - only in Android
	public static final int TEX_CELLS3 = 75;
	public static final int TEX_ALIEN_PURPLE = 76;
	public static final int TEX_ALIEN_GREEN = 77; // Not used - only in Android
	public static final int TEX_CARVED_SANDSTONE = 78;
	public static final int TEX_HAZARD_TOP = 79; // Not used - only in Android
	public static final int TEX_HAZARD_LEFT = 80; // Not used - only in Android
	public static final int TEX_HAZARD_RIGHT = 81; // Not used - only in Android
	public static final int TEX_STAIRS_UP = 82; // Not used - only in Android
	public static final int TEX_STAIRS_DOWN = 83; // Not used - only in Android
	public static final int TEX_POWER_UNIT = 84;
	public static final int TEX_MEDI_BAY = 85;
	public static final int TEX_CLONE_GENERATOR = 86;
	public static final int TEX_GUN_VENDING_MACHINE = 87;
	public static final int TEX_GRENADE_VENDING_MACHINE = 88;
/*	private static final int TEX_RUBBLE_1_TRANSP = 89; // Not used - only in Android
	private static final int TEX_RUBBLE_2_TRANSP = 90; // Not used - only in Android
	private static final int TEX_RUBBLE_3_TRANSP = 91; // Not used - only in Android
	private static final int TEX_RUBBLE_4_TRANSP = 92; // Not used - only in Android*/
	public static final int TEX_ALIEN_COLONY = 93;
	public static final int MEDIKIT = 94;
	public static final int AMMO_PACK = 95;
	public static final int TEX_DOOR = 96;
	public static final int IMG_LASER_BOLT = 97; 
	public static final int TEX_COMPUTER = 98;
	public static final int DESTROYED_COMPUTER = 99;
	public static final int CORPSE_SIDE_1 = 100;
	public static final int CORPSE_SIDE_2 = 101;
	public static final int SHADOW_TOP_TRI = 102;
	public static final int SHADOW_RIGHT_TRI = 103;
	public static final int SHADOW_SQUARE = 104;
	public static final int SHADOW_L_SHAPE= 105;

	public static final int STD_UNIT_S1_E = 106;
	public static final int STD_UNIT_S1_SE = 107;
	public static final int STD_UNIT_S1_S = 108;
	public static final int STD_UNIT_S1_SW = 109;
	public static final int STD_UNIT_S1_W = 110;
	public static final int STD_UNIT_S1_NW = 111;
	public static final int STD_UNIT_S1_N = 112;
	public static final int STD_UNIT_S1_NE = 113;
	
	public static final int STD_UNIT_S2_E = 114;
	public static final int STD_UNIT_S2_SE = 115;
	public static final int STD_UNIT_S2_S = 116;
	public static final int STD_UNIT_S2_SW = 117;
	public static final int STD_UNIT_S2_W = 118;
	public static final int STD_UNIT_S2_NW = 119;
	public static final int STD_UNIT_S2_N = 120;
	public static final int STD_UNIT_S2_NE = 121;
	
	public static final int STERNER_E = 122;
	public static final int STERNER_SE = 123;
	public static final int STERNER_S = 124;
	public static final int STERNER_SW = 125;
	public static final int STERNER_W = 126;
	public static final int STERNER_NW = 127;
	public static final int STERNER_N = 128;
	public static final int STERNER_NE = 129;
	
	// Doors
	public static final byte DOOR_NS = 1;
	public static final byte DOOR_EW = 2;

	// Scenery codes
	public static final byte FLOWERPOT = 1;
	public static final byte FLOWERPOT2 = 2;
	public static final byte BOOKSHELF = 3;
	public static final byte CHAIR = 4;
	public static final byte COMPUTER_TABLE = 5;
	public static final byte DESK = 6;
	public static final byte METAL_SHELF = 7;
	public static final byte SMOKE = 8;
	public static final byte SPARKS = 9;
	public static final byte HEDGEROW = 10;
	public static final byte SECTOR1 = 11;
	public static final byte SMALL_POST = 12;
	public static final byte GAS_CANNISTER = 13;
	public static final byte SECTOR2 = 14;
	public static final byte SECTOR3 = 15;
	public static final byte SECTOR4 = 16;
	public static final byte BRICK_WALL = 17;
	public static final byte PIPES_L = 18;
	public static final byte PIPES_R = 19;
	public static final byte RUBBLE = 20;
	public static final byte RUBBLE_RED = 21;
	public static final byte RUBBLE_YELLOW = 22;
	public static final byte RUBBLE_WHITE = 23;
	public static final byte CHAIR_T = 24;
	public static final byte CHAIR_B = 25;
	public static final byte CHAIR2_L = 26;
	public static final byte CHAIR2_R = 27;
	public static final byte CRYO_CHAMBER = 28;
	public static final byte COMPUTER_SCREEN1 = 29;
	public static final byte PLANT3 = 30;
	public static final byte RED_WALL_PANEL = 31;
	public static final byte GREEN_WALL_PANEL = 32;
	public static final byte DAMAGED_FLOOR = 33;
	public static final byte DAMAGED_FLOOR2 = 34;
	public static final byte GRILL = 35;
	public static final byte SINGLE_PIPE_L = 36;
	public static final byte SINGLE_PIPE_R = 37;
	public static final byte HORIZONTAL_PIPE = 38;
	public static final byte PLANT4 = 39;
	public static final byte PLANT5 = 40;
	public static final byte PLANT6 = 41;
	public static final byte PLANT7 = 42;
	public static final byte REPLICATOR = 43;
	public static final byte CRACK1 = 44;
	public static final byte CRACK2 = 45;
	public static final byte WEED1 = 46;
	public static final byte WEED2 = 47;
	public static final byte GRAFFITI_DIE = 48;
	public static final byte GRAFFITI_HELP = 49;
	public static final byte GRAFFITI_BRAINS = 50;
	public static final byte BARREL2 = 51;
	public static final byte DAMAGED_FLOOR3 = 52;
	public static final byte DAMAGED_FLOOR4 = 53;
	public static final byte DAMAGED_FLOOR5 = 54;
	public static final byte DAMAGED_FLOOR6 = 55;
	public static final byte DAMAGED_FLOOR7 = 56;
	public static final byte BED_TOP = 57;
	public static final byte BED_BOTTOM = 58;
	
	public ImageCodes() {
	}

	public static String GetFilename(int tex_code) {
		switch(tex_code) {
		case TEX_INTERIOR1:
			return "metalfloor1.jpg";
		case TEX_INTERIOR2:
			return "floor3.jpg";
		case TEX_MOONROCK:
			return "moonrock.png";
		case TEX_CORRIDOR1:
			return "corridor.jpg";
		case TEX_RUBBLE:
			return "rubble.png";
		case TEX_PATH:
			return "road1.png";
		case TEX_GRASS:
			//return "grass.png";
			return "grass.jpg";
		case TEX_ROAD:
			return "road2.png";
		case TEX_MUD:
			return "mud.png";
		case TEX_WOODEN_FLOOR:
			return "floor02.png";
		case TEX_WATER:
			return "water.png";
		case TEX_BEACH:
			return "beach.png";
		case TEX_SAND1:
			return "sand1.png";
		case TEX_SAND2:
			return "sand2.png";
		case TEX_WOODEN_PLANKS:
			return "wooden_planks_lr.jpg";
		case TEX_MOONBASE_BLUE:
			//return "moonbase_ceiling.png";
			//return "bluemetal.png";
			//return "spacewall2.png";
			return "ufo2_03.png";
		case TEX_CORRUGATED_WALL:
			return "wall2.jpg";
		case TEX_MUDTRACK_EW:
			return "mudtrack_ew.jpg";
		case TEX_MUDTRACK_NS:
			return "mudtrack_ns.jpg";
		case TEX_RAILWAY_EW:
			return "railway_ew.jpg";
		case TEX_RAILWAY_NS:
			return "railway_ns.jpg";
		case TEX_MUDTRACKS_RB:
			return "mudtracks_br.png";
		case TEX_MUDTRACKS_LT:
			return "mudtracks_tl.png";
		case TEX_SNOW:
			return "snow.jpg";
		case TEX_SPACEWALL:
			return "spacewall.png";
		case TEX_STONE_TILES:
			return "stone_tiles.jpg";
		case TEX_MUDTRACKS_RT:
			return "mudtracks_tr.png";
		case TEX_CARPET1:
			return "carpet006.jpg";
		case TEX_WOODEN_FLOOR2:
			return "wood_b_9.jpg";
		case TEX_SECTOR1:
			return "sector1.png";
		case TEX_STREET:
			return "street001.jpg";
		case TEX_STREET_ZEBRA_UD:
			return "street010_ud.jpg";
		case TEX_STREET_ZEBRA_LR:
			return "street010_lr.jpg";
		case TEX_MUDTRACKS_UD:
			return "mudtracks_tb.png";
		case TEX_MUDTRACKS_UDR:
			return "mudtracks_tbr.png";
		case TEX_MUDTRACKS_LR:
			return "mudtracks_lr.png";
		case TEX_MUDTRACKS_LB:
			return "mudtracks_bl.png";
		case TEX_YELLOW_LINE_INNER_TR:
			return "2ln_crn_ne_g.png";
		case TEX_YELLOW_LINE_INNER_TL:
			return "2ln_crn_nw_g.png";
		case TEX_YELLOW_LINE_INNER_BR:
			return "2ln_crn_se_g.png";
		case TEX_YELLOW_LINE_INNER_BL:
			return "2ln_crn_sw_g.png";
		case TEX_YELLOW_LINE_OUTER_BL:
			return "2ln_crnw_ne_g.png";
		case TEX_YELLOW_LINE_OUTER_BR:
			return "2ln_crnw_nw_g.png";
		case TEX_YELLOW_LINE_OUTER_TL:
			return "2ln_crnw_se_g.png";
		case TEX_YELLOW_LINE_OUTER_TR:
			return "2ln_crnw_sw_g.png";
		case TEX_YELLOW_LINE_L:
			return "2ln_l.png";
		case TEX_YELLOW_LINE_R:
			return "2ln_r.png";
		case TEX_YELLOW_LINE_T:
			return "2ln_t.png";
		case TEX_YELLOW_LINE_B:
			return "2ln_b.png";
		case TEX_ALIEN_SKIN:
			return "alienskin2.jpg";
		case TEX_BULKHEAD:
			return "bulkhead.jpg";
		case TEX_SPACESHIP_WALL:
			return "spaceship_wall.png";
		case TEX_TELEPORTER:
			return "teleporter.jpg";
		case TEX_COUCH:
			return "couch_e.jpg";
		case TEX_METAL_FLOOR5:
			return "floor5.jpg";
		case TEX_METAL_FLOOR6:
			return "floor006.png";
		case TEX_METAL_FLOOR15:
			return "floor015.png";
		case TEX_HEDGE:
			return "hedge02.jpg";
		case TEX_FLOORHATCH:
			return "floorhatch.png";
		case TEX_METAL_FLOOR41:
			return "floor0041.png";
		case TEX_LAB_FLOOR1:
			return "lab_floor1.jpg";
		case TEX_LAB_FLOOR2:
			return "lab_floor2.png";
		case TEX_ROAD_WHITE_LINE_LR:
			return "street001_wl_lr.jpg";
		case TEX_ROAD_WHITE_LINE_UD:
			return "street001_wl_ud.jpg";
		case TEX_SECTOR2:
			return "sector2.png";
		case TEX_SECTOR3:
			return "sector3.png";
		case TEX_SECTOR4:
			return "sector4.png";
		case TEX_ESCAPE_HATCH:
			return "escape_hatch.jpg";
		case TEX_BRICKS:
			return "bricks.jpg";
		case TEX_WHITE_LINE_LR:
			return "white_line_lr.png";
		case TEX_WHITE_LINE_UD:
			return "white_line_ud.png";
		case TEX_HAZARD_TOP:
			return "hazard_top.png";
		case TEX_HAZARD_LEFT:
			return "hazard_left.png";
		case TEX_HAZARD_RIGHT:
			return "hazard_right.png";
		case TEX_STAIRS_UP:
			return "stairs_up.jpg";
		case TEX_STAIRS_DOWN:
			return "stairs_down.jpg";
		case TEX_DOOR:
			return "door_lr.png";
		case IMG_LASER_BOLT:
			return "laser_bolt.png";
		case TEX_COMPUTER:
			return "computerconsole1.png";
		case MEDIKIT:
			return "medikit.png";
		case DESTROYED_COMPUTER:
			return "damaged_computer.jpg";
		case CORPSE_SIDE_1:
			return "corpse1.png";
		case CORPSE_SIDE_2:
			return "corpse2.png";
		case SHADOW_TOP_TRI: 
			return "shadow_top.png";
		case SHADOW_RIGHT_TRI: 
			return "shadow_right.png";
		case SHADOW_SQUARE: 
			return "shadow_square.png";
		case SHADOW_L_SHAPE: 
			return "shadow_l_shape.png";

		case STERNER_E:
			return "sterner_e.png";
		case STERNER_SE:
			return "sterner_se.png";
		case STERNER_S:
			return "sterner_s.png";
		case STERNER_SW:
			return "sterner_sw.png";
		case STERNER_W:
			return "sterner_w.png";
		case STERNER_NW:
			return "sterner_nw.png";
		case STERNER_N:
			return "sterner_n.png";
		case STERNER_NE:
			return "sterner_ne.png";
			
		case STD_UNIT_S1_E:
			return "human_s1_e.png";
		case STD_UNIT_S1_SE:
			return "human_s1_se.png";
		case STD_UNIT_S1_S:
			return "human_s1_s.png";
		case STD_UNIT_S1_SW:
			return "human_s1_sw.png";
		case STD_UNIT_S1_W:
			return "human_s1_w.png";
		case STD_UNIT_S1_NW:
			return "human_s1_nw.png";
		case STD_UNIT_S1_N:
			return "human_s1_n.png";
		case STD_UNIT_S1_NE:
			return "human_s1_ne.png";
			
		case STD_UNIT_S2_E:
			return "human_s2_e.png";
		case STD_UNIT_S2_SE:
			return "human_s2_se.png";
		case STD_UNIT_S2_S:
			return "human_s2_s.png";
		case STD_UNIT_S2_SW:
			return "human_s2_sw.png";
		case STD_UNIT_S2_W:
			return "human_s2_w.png";
		case STD_UNIT_S2_NW:
			return "human_s2_nw.png";
		case STD_UNIT_S2_N:
			return "human_s2_n.png";
		case STD_UNIT_S2_NE:
			return "human_s2_ne.png";
			
		default:
			throw new RuntimeException("Unknown image code: " + tex_code);
			//System.err.println("Warning: Unknown texture code: " + tex_code);
			//return "metalfloor1.jpg";
		}
	}


	public static String GetSceneryFilename(int code) { 
		switch (code) {
		case FLOWERPOT: return "plant.png";
		case FLOWERPOT2: return "plant2.png";
		case DESK: return "desk_s.png";
		case PIPES_L: return "pipes_l.png";
		case PIPES_R: return "pipes_r.png";
		case RUBBLE: return "";
		case RUBBLE_RED: return "";
		case RUBBLE_YELLOW: return "";
		case RUBBLE_WHITE: return "";
		case CHAIR_T: return "chair_t.png";
		case CHAIR_B: return "chair_b.png";
		case CHAIR2_L: return "chair2_l.png";
		case CHAIR2_R: return "chair2_r.png";
		case CRYO_CHAMBER: return "cryo_chamber.png";
		case COMPUTER_SCREEN1: return "";
		case PLANT3: return "plant3.png";
		case RED_WALL_PANEL: return "";
		case GREEN_WALL_PANEL: return "green_wall_panel.png";
		case DAMAGED_FLOOR: return "damaged_floor.png";
		case DAMAGED_FLOOR2: return "damaged_floor2.png";
		case DAMAGED_FLOOR3: return "damaged_floor3.png";
		case DAMAGED_FLOOR4: return "damaged_floor4.png";
		case DAMAGED_FLOOR5: return "damaged_floor5.png";
		case DAMAGED_FLOOR6: return "damaged_floor6.png";
		case DAMAGED_FLOOR7: return "damaged_floor7.png";
		case GRILL: return "grill.png";
		case SINGLE_PIPE_L: return "";
		case SINGLE_PIPE_R: return "";
		case HORIZONTAL_PIPE: return "";
		case PLANT4: return "plant4.png";
		case PLANT5: return "plant5.png";
		case PLANT6: return "plant6.png";
		case PLANT7: return "plant7.png";
		case REPLICATOR: return "";
		case CRACK1: return "crack1.png";
		case CRACK2: return "crack2.png";
		case WEED1: return "";
		case WEED2: return "";
		case GRAFFITI_DIE: return "";
		case GRAFFITI_HELP: return "";
		case GRAFFITI_BRAINS: return "";
		case BED_TOP: return "bed_top.png";
		case BED_BOTTOM: return "bed_bottom.png";
		case BOOKSHELF: return "bookshelf_s.png";
		default:
			return "";
		}
	}
	
	
	public static int GetCorpseForSide(byte side) {
		switch (side) {
		case 1: return CORPSE_SIDE_1;
		case 2: return CORPSE_SIDE_2;
		default: throw new RuntimeException("Can't handle side " + side);
		}
	}
}
