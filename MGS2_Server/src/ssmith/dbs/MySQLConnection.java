package ssmith.dbs;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class MySQLConnection {

	private Connection conn;
	public String server, dbs, login, pwd;

	public MySQLConnection() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		super();
		Class.forName("com.mysql.jdbc.Driver").newInstance();
	}


	public ResultSet getResultSet(String sql) throws SQLException {
		Statement stmt = (Statement)conn.createStatement();

		// Execute the query
		try {
			ResultSet rs = (ResultSet) stmt.executeQuery(sql);
			return rs;
		} catch (SQLException e) {
			throw new SQLException("Error performing " + sql + ": " + e);
		}
	}


	public void connect(String server, String dbs, String login, String pwd) throws SQLException {
		this.server = server;
		this.dbs = dbs;
		this.login = login;
		this.pwd = pwd;
		conn = (Connection) DriverManager.getConnection("jdbc:mysql://" + server + "/" + dbs + "?" + "user=" + login + "&password=" + pwd);
	}


	public boolean doesDatabaseExist(String name) throws SQLException {
		ResultSet rs = null;
		try {
			rs = getResultSet("SHOW DATABASES LIKE " + SQLFuncs.s2sql(name));
			if (rs.next()) {
				return true;
			} else {
				rs.close();
				// Check for lowercase version now
				rs = getResultSet("SHOW DATABASES LIKE " + SQLFuncs.s2sql(name.toLowerCase()));
				return rs.next();			
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
		}
	}


	public boolean doesTableExist(String name) throws SQLException {
		ResultSet rs = getResultSet("SHOW TABLE STATUS LIKE " + SQLFuncs.s2sql(name));
		return rs.next();
	}


	public boolean doesIndexExist(String name, String idx) throws SQLException {
		ResultSet rs = null;
		try {
		rs = getResultSet("SHOW INDEXES FROM " + name + " WHERE Key_name = " + SQLFuncs.s2sql(idx));
		return rs.next();
		} finally {
			rs.close();
		}
	}


	public boolean doesColumnExist(String table, String col) throws SQLException {
		ResultSet rs = getResultSet("SHOW COLUMNS FROM " + table + " WHERE Field = " + SQLFuncs.s2sql(col));
		return rs.next();
	}


	public synchronized int RunIdentityInsert_Syncd(String sql) throws SQLException {
		Statement stmt = (Statement) conn.createStatement();
		//stmt.execute(sql);
		//int id = getScalarAsInt("SELECT @@IDENTITY AS NewID");
		//int id = (int)stmt.getLastInsertID();
		stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
		ResultSet rs = stmt.getGeneratedKeys();
		rs.next();
		int id = rs.getInt(1);
		rs.close();
		if (stmt != null) {
			stmt.close();
		}
		return id;
	}


	public void RunIdentityInsert(String sql) throws SQLException {
		//long start = System.currentTimeMillis();
		Statement stmt = (Statement) conn.createStatement();
		stmt.execute(sql);
		//DSRWebServer.p("Insert took " + (System.currentTimeMillis() - start));
	}


	public void runSQL(String sql) throws SQLException {
		Statement stmt = null;
		try {
			stmt = (Statement) conn.createStatement();
			stmt.execute(sql);
		} catch (SQLException ex) {
			throw new SQLException("Error running " + sql + ": " + ex.getMessage());
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}


	public int runSQLUpdate(String sql) throws SQLException {
		Statement stmt = null;
		try {
			stmt = (Statement) conn.createStatement();
			return stmt.executeUpdate(sql);
		} catch (SQLException ex) {
			throw new SQLException("Error running " + sql + ": " + ex.getMessage());
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}


	public int runSQLDelete(String sql) throws SQLException {
		Statement stmt = null;
		try {
			stmt = (Statement) conn.createStatement();
			return stmt.executeUpdate(sql);
		} catch (SQLException ex) {
			throw new SQLException("Error running " + sql + ": " + ex.getMessage());
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}


	public int getScalarAsInt(String sql) throws SQLException {
		/*try {
			SQLCommandsTable.Add(sql)	;
		} catch (SQLException ex) {
			throw new SQLException("Error running " + sql + ": " + ex.getMessage());
		}*/

		try {
			ResultSet rs = this.getResultSet(sql);
			rs.next();
			int val = rs.getInt(1);
			rs.close();
			return val;
		} catch (SQLException ex) {
			throw new SQLException("Error running " + sql + ": " + ex.getMessage());
		}
	}


	public Date getScalarAsDate(String sql) throws SQLException {
		try {
			ResultSet rs = this.getResultSet(sql);
			rs.next();
			Date d = rs.getTimestamp(1);
			rs.close();
			return d;
		} catch (SQLException ex) {
			throw new SQLException("Error running " + sql + ": " + ex.getMessage());
		}
	}


	public String getScalarAsString(String sql) throws SQLException {
		/*try {
			SQLCommandsTable.Add(sql)	;
		} catch (SQLException ex) {
			throw new SQLException("Error running " + sql + ": " + ex.getMessage());
		}*/

		try {
			ResultSet rs = this.getResultSet(sql);
			rs.next();
			String val = rs.getString(1);
			rs.close();
			return val;
		} catch (SQLException ex) {
			throw new SQLException("Error running " + sql + ": " + ex.getMessage());
		}
	}


	public void close() throws SQLException {
		conn.close() ;
	}


	public String getListAsString(String sql) throws SQLException {
		/*try {
			SQLCommandsTable.Add(sql)	;
		} catch (SQLException ex) {
			throw new SQLException("Error running " + sql + ": " + ex.getMessage());
		}*/

		ResultSet rs = this.getResultSet(sql);
		StringBuffer str = new StringBuffer();
		while (rs.next()) {
			str.append(rs.getObject(1) + ",");
		}
		if (str.length() > 0) {
			str.delete(str.length()-1, str.length());
		}
		return str.toString();
	}

}
