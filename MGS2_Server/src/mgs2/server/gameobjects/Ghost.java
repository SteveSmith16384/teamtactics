package mgs2.server.gameobjects;

import java.io.IOException;
import mgs2.server.ServerGame;
import mgs2.shared.ImageCodes;
import mgs2.shared.Statics;

public class Ghost extends AbstractMonster {

	public Ghost(ServerGame _main, int _x, int _y) throws IOException {
		super(_main, "Ghost", _x, _y, Statics.SQ_SIZE, Statics.SQ_SIZE, ImageCodes.CHAIR, 10);
	}


	public void process(long interpol) throws IOException {

	}

}
