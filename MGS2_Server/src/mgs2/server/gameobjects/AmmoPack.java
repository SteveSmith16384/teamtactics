package mgs2.server.gameobjects;

import java.io.IOException;

import mgs2.server.ServerGame;
import mgs2.shared.ImageCodes;

public class AmmoPack extends AbstractPickup {

	public AmmoPack(ServerGame _main, int _x, int _y) throws IOException {
		super(_main, "AmmoPack", Type.AMMO_PACK, _x, _y, ImageCodes.AMMO_PACK);
	}

	@Override
	public void pickedUp(PlayersAvatar player) {
		
	}

}
