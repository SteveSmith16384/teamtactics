package mgs2.server.gameobjects;

import java.io.IOException;

import mgs2.server.ServerGame;
import mgs2.server.ServerGameObject;

public abstract class AbstractMonster extends ServerGameObject {

	public AbstractMonster(ServerGame _main, String _name, int _x, int _y, int w, int h, int img_code, int health) throws IOException {
		super(_main, _name, Type.ENEMY, _x, _y, w, h, true, true, img_code, health, (byte)2, true, true);
	}

}
