package mgs2.server.gameobjects;

import java.awt.Point;
import java.io.IOException;

import mgs2.server.ServerGame;
import mgs2.server.ServerGameObject;
import mgs2.shared.Statics;
import ssmith.util.Interval;

public class MonsterGenerator extends ServerGameObject {

	private static final int HEALTH = 10;
	
	private Interval generate_int = new Interval(10000);
	
	public MonsterGenerator(ServerGame _main, int _x, int _y, int img_code) throws IOException {
		super(_main, "MonsterGenerator", Type.MONSTER_GEN, _x, _y, Statics.SQ_SIZE, Statics.SQ_SIZE, true, true, img_code, HEALTH, (byte)2, true, true);
	}


	public void process(long interpol) throws IOException {
		if (generate_int.hitInterval()) {
			if (game.spritegrid2.isSquareClear(new Point(super.gridx+1, super.gridy))) {
				Ghost ghost = new Ghost(game, (this.gridx+1)* Statics.SQ_SIZE, this.gridy * Statics.SQ_SIZE);
				game.main.broadcastNewObject(ghost);
			}
		}
	}

	
}
