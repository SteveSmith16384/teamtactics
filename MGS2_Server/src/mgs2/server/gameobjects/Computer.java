package mgs2.server.gameobjects;

import java.io.IOException;

import mgs2.server.ServerGame;
import mgs2.server.ServerGameObject;
import mgs2.shared.ImageCodes;
import mgs2.shared.Statics;

public class Computer extends ServerGameObject {
	
	public Computer(ServerGame _main, int _x, int _y, int img_code, byte side) throws IOException {
		super(_main, "Computer", Type.COMPUTER, _x, _y, Statics.SQ_SIZE, Statics.SQ_SIZE, true, true, img_code, Statics.COMPUTER_HEALTH, side, false, true);
		
	}

	
	@Override
	protected void destroyed(ServerGameObject by) throws IOException {
		game.main.broadcastMsg(by.getName() + " has destroyed a computer!");
		game.mission.computerDestroyed(this);
		super.destroyed(by);
		
		// Replace with wall
		Wall wall = new Wall(game, (int)this.getX(), (int)this.getY(), ImageCodes.DESTROYED_COMPUTER);
		game.main.broadcastNewObject(wall);
		
	}


}
