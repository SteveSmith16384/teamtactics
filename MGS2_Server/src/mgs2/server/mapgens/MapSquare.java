package mgs2.server.mapgens;

public final class MapSquare {

	public byte x, y, owner_side, major_type;
	public short image_code;
	public short raised_image_code;
	public byte door_type = -1; // See MapDataTable
	public byte deploy_sq_side = -1;
	public byte escape_hatch_side = -1;
	public int destroyed = 0;
	public boolean door_open = false;
	public short scenery_code; // see MapDataTable
	public byte scenery_direction;
	public byte smoke_type;

	public MapSquare(byte _x, byte _y) {
		super();

		x = _x;
		y = _y;
	}

}
