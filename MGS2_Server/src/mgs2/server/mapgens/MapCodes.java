package mgs2.server.mapgens;

public final class MapCodes {

	// Major types
	public static final byte MT_NOTHING = 0;
	public static final byte MT_FLOOR = 1;
	public static final byte MT_COMPUTER = 2;
	public static final byte MT_WALL = 3;
	public static final byte MT_MONSTER_GEN = 4;

	// Doors
	public static final byte DOOR_NS = 1;
	public static final byte DOOR_EW = 2;

	// Scenery codes
	public static final byte FLOWERPOT = 1;
	public static final byte FLOWERPOT2 = 2;
	public static final byte BOOKSHELF = 3;
	public static final byte CHAIR = 4;
	public static final byte COMPUTER_TABLE = 5;
	public static final byte DESK = 6;
	public static final byte METAL_SHELF = 7;
	public static final byte SMOKE_FX = 8;
	public static final byte SPARKS = 9;
	public static final byte HEDGEROW = 10;
	public static final byte SECTOR1 = 11;
	public static final byte SMALL_POST = 12;
	public static final byte BARREL1 = 13;
	public static final byte SECTOR2 = 14;
	public static final byte SECTOR3 = 15;
	public static final byte SECTOR4 = 16;
	public static final byte BRICK_WALL = 17;
	public static final byte PIPES_L = 18;
	public static final byte PIPES_R = 19;
	public static final byte RUBBLE = 20;
	public static final byte RUBBLE_RED = 21;
	public static final byte RUBBLE_YELLOW = 22;
	public static final byte RUBBLE_WHITE = 23;
	public static final byte CHAIR_T = 24;
	public static final byte CHAIR_B = 25;
	public static final byte CHAIR2_L = 26;
	public static final byte CHAIR2_R = 27;
	public static final byte CRYO_CHAMBER = 28;
	public static final byte COMPUTER_SCREEN1 = 29;
	public static final byte PLANT3 = 30;
	public static final byte RED_WALL_PANEL = 31;
	public static final byte GREEN_WALL_PANEL = 32;
	public static final byte DAMAGED_FLOOR = 33;
	public static final byte DAMAGED_FLOOR2 = 34;
	public static final byte GRILL = 35;
	public static final byte SINGLE_PIPE_L = 36;
	public static final byte SINGLE_PIPE_R = 37;
	public static final byte HORIZONTAL_PIPE = 38;
	public static final byte PLANT4 = 39;
	public static final byte PLANT5 = 40;
	public static final byte PLANT6 = 41;
	public static final byte PLANT7 = 42;
	public static final byte REPLICATOR = 43;
	public static final byte CRACK1 = 44;
	public static final byte CRACK2 = 45;
	public static final byte WEED1 = 46;
	public static final byte WEED2 = 47;
	public static final byte GRAFFITI_DIE = 48;
	public static final byte GRAFFITI_HELP = 49;
	public static final byte GRAFFITI_BRAINS = 50;
	public static final byte BARREL2 = 51;
	public static final byte DAMAGED_FLOOR3 = 52;
	public static final byte DAMAGED_FLOOR4 = 53;
	public static final byte DAMAGED_FLOOR5 = 54;
	public static final byte DAMAGED_FLOOR6 = 55;
	public static final byte DAMAGED_FLOOR7 = 56;
	public static final byte BED_TOP = 57;
	public static final byte BED_BOTTOM = 58;
	
	private MapCodes() {

	}

}
