package mgs2.server;

import java.awt.Point;
import java.util.ArrayList;

import mgs2.server.mapgens.MapSquare;
import ssmith.lang.NumberFunctions;

public class ServerMapData {

	public byte width, height;
	public MapSquare map[][];
	private ArrayList<Point>[] deploy_squares = new ArrayList[3];

	public ServerMapData(byte w, byte h) {
		super();

		width = w;
		height = h;

		map = new MapSquare[w][h];
	}


	public synchronized Point getRandomPlayerStartPosition(ServerSpriteGrid grid, byte side) {
		if (deploy_squares[side] == null) {
			deploy_squares[side] = new ArrayList<Point>();
			for (byte y=0 ; y<height ; y++) {
				for (byte x=0 ; x<width ; x++) {
					MapSquare sq = map[x][y];
					if (sq.deploy_sq_side == side) {
						deploy_squares[side].add(new Point(x, y));
					}
				}
			}
			if (deploy_squares[side].isEmpty()) {
				throw new RuntimeException("No deploy squares for side " + side);
			}
		}
		ArrayList<Point> p = deploy_squares[side];

		// Check there's not someone stood there
		while (true) {
			Point p2 = p.get(NumberFunctions.rnd(0, p.size()-1));
			boolean clear = grid.isSquareClear(p2);
			if (clear) {
				return p2;
			}
		}
	}
	
	


}
