package mgs2.server;

import java.io.IOException;

import mgs2.server.gameobjects.AbstractPickup;
import mgs2.server.gameobjects.Bullet;
import mgs2.server.gameobjects.Computer;
import mgs2.server.gameobjects.Door;
import mgs2.server.gameobjects.PlayersAvatar;
import mgs2.shared.AbstractGameObject;
import mgs2.shared.AbstractGameObject.Type;
import mgs2.shared.ClientOnlyObjectType;
import mgs2.shared.Statics;
import mgs2.shared.UnitType;
import ssmith.lang.GeometryFuncs;

public final class CollisionLogic {

	private CollisionLogic() {
	}


	public static void collision(ServerMain main, AbstractGameObject o1, AbstractGameObject o2) throws IOException {
		if (o1 == null || o2 == null) {
			ServerMain.p("Null object for collision");
			return;
		}
		// Bullets and players
		if (o1.type == Type.BULLET && o2.type == Type.PLAYER) {
			collisionBulletPlayer(main, (Bullet)o1, (PlayersAvatar)o2);
		} else if (o1.type == Type.PLAYER && o2.type == Type.BULLET) {
			collisionBulletPlayer(main, (Bullet)o2, (PlayersAvatar)o1);

			// Bullets and computer
		} else if (o1.type == Type.BULLET && o2.type == Type.COMPUTER) {
			collisionBulletComputer(main, (Bullet)o1, (Computer)o2);
		} else if (o1.type == Type.COMPUTER && o2.type == Type.BULLET) {
			collisionBulletComputer(main, (Bullet)o2, (Computer)o1);

			// Players and doors
		} else if (o1.type == Type.PLAYER && o2.type == Type.DOOR) {
			collisionPlayerDoor(main, (PlayersAvatar)o1, (Door)o2);
		} else if (o1.type == Type.DOOR && o2.type == Type.PLAYER) {
			collisionPlayerDoor(main, (PlayersAvatar)o2, (Door)o1);

			// Players and medikits
		} else if (o1.type == Type.PLAYER && o2 instanceof AbstractPickup) {
			collisionPlayerAndPickup(main, (PlayersAvatar)o1, (AbstractPickup)o2);
		} else if (o1 instanceof AbstractPickup && o2.type == Type.PLAYER) {
			collisionPlayerAndPickup(main, (PlayersAvatar)o2, (AbstractPickup)o1);
		}
	}


	private static void collisionBulletPlayer(ServerMain main, Bullet bullet, PlayersAvatar player) throws IOException {
		if (player.getHealth() > 0) {
			// check sides
			if (bullet.side != player.side) {
				int damage = GetBulletDamage(bullet);
				float adj = GetAdjustmentForAngle(bullet.angle, player.angle);
				if (Statics.DEBUG) {
					System.out.println("adj=" + adj);
				}
				damage = (int)(damage * adj);
				if (damage < 1) {
					damage = 1;
				}
				player.damage(damage, bullet.controlled_by);
				//main.broadcastClientOnlyObject(ClientOnlyObjectType.SHOT_EXPLOSION, (int)player.getX(), (int)player.getY());  No since we don't know exactly where
			} else { // Same side 
				if (bullet.controlled_by.getUnitType() == UnitType.DOCTOR) {
					player.incHealth(1);
				}
			}
		}
	}


	private static float GetAdjustmentForAngle(float source_ang, float target_ang) {
		float ang_diff = GeometryFuncs.GetDiffBetweenAngles(source_ang, target_ang); // todo - make it an algorythm
		if (ang_diff >= 130) {
			return .5f;//1f;
		} else if (ang_diff >= 40) {
			return 0.75f;
		} else {
			return 1f;//.25f;
		}
	}


	private static void collisionBulletComputer(ServerMain main, Bullet bullet, Computer computer) throws IOException {
		if (bullet.side != computer.side) {
			int damage = GetBulletDamage(bullet);
			computer.damage(damage, bullet.controlled_by);
			main.broadcastClientOnlyObject(ClientOnlyObjectType.COMPUTER_EXPLOSION, (int)computer.getX() +  (computer.width/2), (int)computer.getY() +  (computer.height/2));
		}
	}


	private static void collisionPlayerDoor(ServerMain main, PlayersAvatar player, Door door) throws IOException {
		door.startOpening(player);
	}


	private static void collisionPlayerAndPickup(ServerMain main, PlayersAvatar player, AbstractPickup pickup) throws IOException {
		pickup.removeAndTellClients();
		pickup.pickedUp(player);
	}

	
	private static int GetBulletDamage(Bullet bullet) {
		int damage = bullet.controlled_by.max_bullet_damage - bullet.controlled_by.min_bullet_damage;

		// Adjust damage by distance
		double dist = bullet.distanceTo(bullet.controlled_by);
		double frac = (bullet.controlled_by.bullet_range-dist)/bullet.controlled_by.bullet_range;
		damage = (int)(damage * frac);
		damage += bullet.controlled_by.min_bullet_damage;
		return damage;
	}


}
