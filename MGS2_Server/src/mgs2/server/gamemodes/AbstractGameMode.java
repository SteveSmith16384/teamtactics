package mgs2.server.gamemodes;

import java.io.IOException;
import java.util.ArrayList;

import mgs2.server.PlayerData;
import mgs2.server.ServerGame;
import mgs2.server.ServerMain;
import mgs2.server.ServerMapData;
import mgs2.server.gameobjects.Computer;
import mgs2.server.gameobjects.PlayersAvatar;
import mgs2.shared.Statics;

public abstract class AbstractGameMode {

	protected ServerMain main;
	protected ServerGame game;
	public int type;
	private byte num_sides;
	public long duration;

	public static AbstractGameMode Factory(ServerMain main, ServerGame _game, int type) {
		switch (type) {
		case Statics.GM_THE_ASSASSINS: 
			return new TheAssassins(main, _game);
		case Statics.GM_MOONBASE_ASSAULT: 
			return new MoonbaseAssault(main, _game);
		case Statics.GM_DUNGEON1:
			return new Dungeon1(main, _game);
		default: 
			throw new IllegalArgumentException("Unknown game mode: " + type);
		}
	}


	protected AbstractGameMode(ServerMain _main, ServerGame _game, int _type, byte _num_sides, long _duration) {
		super();

		main = _main;
		game = _game;
		type = _type;
		num_sides = _num_sides;
		duration = _duration;
		if (Statics.DEBUG) {
			duration = 1000*30;
		}
	}


	public byte getNumSides() {
		return num_sides;
	}

	public abstract ServerMapData loadMap() throws IOException;

	public abstract String getName();

	public abstract String getSideName(int side);

	public abstract String getSideObjective(int side);
	
	public abstract ArrayList<String> getHelpText();


	/**
	 * Return the new side, if changed by mission requirements.
	 */
	public void playerJoined(PlayerData player) throws IOException {
		// Override if required

	}
	

	public void gameStarted() throws IOException {
		// Override if required
	}
	
	
	public void playerLeft(PlayerData player) throws IOException {
		// Override if required
	}


	public void process(long interpol) throws IOException {
	}


	public void playerDied(PlayersAvatar avatar) throws IOException {
		// Override if required
	}


	public void timeExpired() throws IOException {
		// Override if required
	}

	
	public void computerDestroyed(Computer comp) throws IOException {
		// Override if required
	}

	public boolean canSideOpenDoor(byte side) {
		return false; // Override if required
	}


}
