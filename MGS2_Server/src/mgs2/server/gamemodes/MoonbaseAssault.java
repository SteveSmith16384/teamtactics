package mgs2.server.gamemodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import mgs2.server.ServerGame;
import mgs2.server.ServerMain;
import mgs2.server.ServerMapData;
import mgs2.server.gameobjects.Computer;
import mgs2.server.gameobjects.PlayersAvatar;
import mgs2.server.mapgens.MapLoader;
import mgs2.shared.Statics;
import ssmith.lang.Dates;

public class MoonbaseAssault extends AbstractGameMode {
	
	private static String HELP_TEXT[] = {"The Attackers must destroy the computers",
		"The defenders must prevent computers being destroyed"};
	
	private static final int COMPS_TO_WIN = 10;

	private static final long DURATION = Dates.MINUTE*5;
	private int comps_destroyed = 0;

	public MoonbaseAssault(ServerMain main, ServerGame game) {
		super(main, game, Statics.GM_MOONBASE_ASSAULT, (byte)2, DURATION);
	}


	public void computerDestroyed(Computer comp) throws IOException {
		comps_destroyed++;
		if (this.comps_destroyed >= COMPS_TO_WIN) {
			game.main.broadcastMsg("The computers have been destroyed!");
			main.game.sideHasWon(1);
		} else {
			game.main.broadcastMsg((COMPS_TO_WIN-comps_destroyed) + " computers remaining");
		}

	}


	@Override
	public String getName() {
		return "Moonbase Assault";
	}


	@Override
	public String getSideName(int side) {
		switch (side) {
		case 1: return "The Saboteurs";
		case 2: return "The Defenders";
		default: throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public void playerDied(PlayersAvatar avatar) throws IOException {
		// Do nothing
	}


	@Override
	public void timeExpired() throws IOException {
		game.main.broadcastMsg("Time has run out!");
		game.sideHasWon(2);
	}


	@Override
	public ServerMapData loadMap() throws IOException {
		return new MapLoader().loadMap("moonbaseassault.csv");
	}


	@Override
	public void gameStarted() throws IOException {
	}


	@Override
	public String getSideObjective(int side) {
		switch (side) {
		case 1: return "You must destroy " + COMPS_TO_WIN + " computers";
		case 2: return "You must defend the computers";
		default:
			throw new IllegalArgumentException("No side " + side);
		}
	}


	@Override
	public boolean canSideOpenDoor(byte side) {
		return side == (byte)2;
	}


	@Override
	public ArrayList<String> getHelpText() {
		return new ArrayList<String>(Arrays.asList(HELP_TEXT));
	}


}
