package mgs2.server.gamemodes;

import java.io.IOException;
import java.util.ArrayList;

import mgs2.server.ServerGame;
import mgs2.server.ServerMain;
import mgs2.server.ServerMapData;
import mgs2.server.mapgens.MapCodes;
import mgs2.server.mapgens.MapSquare;
import mgs2.shared.ImageCodes;
import mgs2.shared.Statics;

public class Dungeon1 extends AbstractGameMode {

	private static final byte WIDTH = 60;
	private static final byte HEIGHT = 60;
	
	private static final long DURATION = 1000*60*5;
	
	public Dungeon1(ServerMain _main, ServerGame _game) {
		super(_main, _game, Statics.GM_DUNGEON1, (byte)1, DURATION);
	}

	
	@Override
	public ServerMapData loadMap() throws IOException {
		ServerMapData mapdata = new ServerMapData(WIDTH, HEIGHT);
		
		for (byte y=0 ; y<HEIGHT ; y++) {
			for (byte x=0 ; x<WIDTH ; x++) {
				MapSquare sq = new MapSquare(x, y);
				if (x == 0 || y == 0 || x == WIDTH-1 || y == HEIGHT-1) {
					sq.major_type = MapCodes.MT_WALL;
					sq.image_code = ImageCodes.BRICK_WALL;
				} else {
					sq.major_type = MapCodes.MT_FLOOR;
					sq.image_code = ImageCodes.TEX_SAND1;
				}
				mapdata.map[x][y] = sq;
			}
		}
		mapdata.map[2][2].deploy_sq_side = 1;

		mapdata.map[12][12].major_type = MapCodes.MT_MONSTER_GEN;
		
		return mapdata;
	}

	@Override
	public String getName() {
		return "Test Dungeon 1";
	}

	@Override
	public String getSideName(int side) {
		return "The Party";
	}

	@Override
	public String getSideObjective(int side) {
		return "To win";
	}

	@Override
	public ArrayList<String> getHelpText() {
		return new ArrayList();
	}

}
