package mgs2.server.gamemodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import mgs2.server.PlayerData;
import mgs2.server.ServerGame;
import mgs2.server.ServerMain;
import mgs2.server.ServerMapData;
import mgs2.server.gameobjects.PlayersAvatar;
import mgs2.server.mapgens.MapLoader;
import mgs2.shared.GameStage;
import mgs2.shared.Statics;
import ssmith.lang.Dates;
import ssmith.lang.NumberFunctions;

public final class TheAssassins extends AbstractGameMode {

	private static String HELP_TEXT[] = {"The Attackers must kill the target in white",
	"The defenders must defend the target until the end"};

	private PlayersAvatar target = null;
	private static final long DURATION = Dates.MINUTE*5;

	protected TheAssassins(ServerMain main, ServerGame game) {
		super(main, game, Statics.GM_THE_ASSASSINS, (byte)2, DURATION);
	}


	@Override
	public void playerLeft(PlayerData player) throws IOException {
		checkTargetExists();
		if (target == null || player.avatar == target) {
			target = null;
			this.chooseRandomTarget();
		}
	}


	@Override
	public final void playerJoined(PlayerData player) throws IOException {
		if (game.getGameStage() == GameStage.STARTED) {
			checkTargetExists();
			if (player.side == 2 && target == null) {
				chooseRandomTarget();
			}
		}
	}


	private void checkTargetExists() {
		if (target != null) {
			if (main.game.spritegrid2.doesObjectExist(target) == false) {
				target = null;
			}
		}
	}



	@Override
	public String getName() {
		return "The Assassins";
	}


	@Override
	public String getSideName(int side) {
		switch (side) {
		case 1: return "The Assassins";
		case 2: return "The Fugitives";
		default: throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public void playerDied(PlayersAvatar avatar) throws IOException {
		if (this.game.getGameStage() == GameStage.STARTED) {
			if (avatar == target) {
				game.main.broadcastMsg("The target has been killed!");
				game.sideHasWon(1);
			}
		}
	}


	@Override
	public void timeExpired() throws IOException {
		game.main.broadcastMsg("Time has run out!");
		game.sideHasWon(2);
	}


	@Override
	public ServerMapData loadMap() throws IOException {
		return new MapLoader().loadMap("theassassins.csv");
	}


	@Override
	public void gameStarted() throws IOException {
		this.chooseRandomTarget();
	}


	private void chooseRandomTarget() throws IOException {
		// Choose a random sterner
		int num = game.players[2].size();
		if (num > 0) {
			PlayerData player = game.players[2].get(NumberFunctions.rnd(0, num-1));
			this.target = player.avatar;
			main.confirmUnitType(player.conn.getDataOutputStream(), player);
			main.broadcastMsg(player.name + " is the target!");
			//main.broadcastChangePlayerImageSet(target);
		} else {
			target = null;
		}
	}


	@Override
	public String getSideObjective(int side) {
		switch (side) {
		case 1: return "You must kill the target - the one in white";
		case 2: return "You must defend the target - the one in white";
		default:
			throw new IllegalArgumentException("No side " + side);
		}
	}


	@Override
	public boolean canSideOpenDoor(byte side) {
		return side == (byte)2;
	}


	@Override
	public ArrayList<String> getHelpText() {
		return new ArrayList<String>(Arrays.asList(HELP_TEXT));
	}


}
