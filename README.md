#Team Tactics

A 2D top-down squad-based realtime multiplayer strategy game loosely based on Laser Squad and Team Fortress.

![alt text](http://3.bp.blogspot.com/-pVYcWWnQNEo/Vpa631YGoxI/AAAAAAAABOk/kKYvVygFbdQ/s1600/tt_ss7.png)

Licence:
All code and assets licenced under MIT (https://opensource.org/licenses/MIT).